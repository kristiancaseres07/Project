import './App.css';
import ContactUs from './pages/ContactUs';
import Home from './pages/Home'
import AppNavBar from './components/AppNavBar'
import Footer from './components/Footer'
import {BrowserRouter as Router} from 'react-router-dom';
import {Routes, Route} from 'react-router-dom'; 
import { Container } from 'react-bootstrap'


function App() {
  return (
    <Router>
    <AppNavBar/>
      <div>
          <Routes>
            <Route path="/" element={<Home/>} />
            <Route path="/contact-us" element={<ContactUs/>} />
          </Routes>
      </div>
    <Footer/>
    </Router>
  );
}

export default App;
